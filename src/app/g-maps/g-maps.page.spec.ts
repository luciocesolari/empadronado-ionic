import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GMapsPage } from './g-maps.page';

describe('GMapsPage', () => {
  let component: GMapsPage;
  let fixture: ComponentFixture<GMapsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GMapsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GMapsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
