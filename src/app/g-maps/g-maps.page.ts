import { Component, OnInit } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';
@Component({
  selector: 'app-g-maps',
  templateUrl: './g-maps.page.html',
  styleUrls: ['./g-maps.page.scss'],
})
export class GMapsPage implements OnInit {
  map: GoogleMap;
  constructor() { }

  ngOnInit() {
    this.loadMap();
  }
  loadMap() {

    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBOLQJr2Ut_0kMehFD_XPKnF_uaOphIhZc',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBOLQJr2Ut_0kMehFD_XPKnF_uaOphIhZc'
    });
    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: 43.0741904,
           lng: -89.3809802
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    let marker: Marker = this.map.addMarkerSync({
      title: 'Ionic',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: 43.0741904,
        lng: -89.3809802
      }
    });
    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      alert('clicked');
    });
  }

}
